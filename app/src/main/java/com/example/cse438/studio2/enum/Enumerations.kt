package com.example.cse438.studio2.enum

enum class UserInterfaceState {
    NETWORK_ERROR,
    NO_DATA,
    RESET,
    HOME,
    RESULTS
}